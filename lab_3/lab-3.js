const EventEmitter = require('events').EventEmitter;
let logger = new EventEmitter();
let users = [];
let msgs = [];

outputMsg = (msg) => {
    console.log('New message: ', msg);
    msgs.push(msg);
};

loginEvent = (name) => {
    console.log('New user: ', name);
    users.push(name);
};

getUsersEvent = () => {
    console.log('Logged users: ', users.join(','));
};

getMessagesEvent = () => {
    console.log('Messages: ', msgs.join(','));
};

logger.on('message', outputMsg);
logger.emit('message', 'Hello world');

logger.on('login', loginEvent);
logger.emit('login', 'Igor');
logger.emit('login', 'Andrey');
logger.emit('login', 'Vika');

logger.on('getUsers', getUsersEvent);
logger.emit('getUsers', 'Vika');
logger.on('getMessages', getMessagesEvent);
logger.emit('getMessages', 'Vika');
