const http = require('http');
const port = 9000;

const requestHandler = (request, response) => {
    if (request.url === '/stop') {
        console.log('Close');
        server.close();
    } else {
        response.statusCode = 200;
        response.write('Hello from Node.js from Alina Tatarinova\n');
        response.end();
    }
};

const requestSecond = (request, response) => {
    if (request.url === '/stop') {
        server.close();
    } else {
        console.log("Url: " + request.url);
        console.log("Тип запроса: " + request.method);
        console.log("Код статуса: " + response.statusCode);
    }
};


const listenToServer = () => {
    console.log(`Listening port: ${port}`);
};

const connectingToServer = () => {
    console.log('Connecting...');
};

const closeServer = () => {
    console.log('The end');
};


const server = http.createServer();
server.on('connection', connectingToServer);
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
}, listenToServer,);
server.on('request', requestHandler);
server.on('request', requestSecond);
server.on('close', closeServer);
