const http = require("http");
const dop1 = require('download_image');
const dop2 = require('../lab_5/logs');

dop2.Warn('Warning!');
dop2.Info('Information');
dop2.Error('Error 404');

const server = http.createServer(function(request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'});
  	const readableStream = dop1.getFile("logo.jpg");
  	readableStream.pipe(response);
});

server.listen(3000);