const http = require('http');
const port = 8080;
const fs = require('fs');

const requestHandler = (request, response) => {
    response.setHeader("Content-Type", "text/html; charset=utf-8;");
    response.write('Hello from Alina\n');
    fs.readFile('./index.html', "UTF8",(err, content) => {
        if (err) {
            console.log('Error happened during reading the file');
            return console.log(err)
        }
        response.end(content);
        // console.log(content);
    });
};

const server = http.createServer(requestHandler);
server.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`Request is successful`);
});
