const Warn = (msg) => console.log('Warn : ', msg);
const Info = (msg) => console.log('Info : ', msg);
const Error_ = (msg) => console.log('Error : ', msg);

module.exports.Warn = Warn;
module.exports.Info = Info;
module.exports.Error = Error_;