const fs = require('fs');

const getFile = (name) => fs.createReadStream(name);

module.exports.getFile = getFile;