const http = require("http");
const dop1 = require('./get-file.js');
const dop2 = require('./logs.js');

dop2.Warn('Warning!');
dop2.Info('Information');
dop2.Error('Error 404');

const server = http.createServer(function(request,response){
	response.writeHead(200, {'Content-Type': 'text/plain'});
  	const readableStream = dop1.getFile("../lab_4/logo.gif");
  	readableStream.pipe(response);
});

server.listen(3000);
