var fs = require('fs');
var EventEmitter = require('events');
var http = require("http");
var logger = new EventEmitter();

console.log("Задание 1: ");
var fileContent = fs.readFileSync("index.html", "utf8");
logger.on('data', () => console.log(fileContent));
logger.emit('data');
logger.removeAllListeners();

console.log("Задание 2: ");
logger.on('data', () => process.stdout.write(fileContent));
logger.emit('data');

console.log("Задание 3: ");
var readableStream = fs.createReadStream("index.html", "utf8");
readableStream.pipe(process.stdout);

var server = http.createServer(function(request,response){
    response.writeHead(200, {'Content-Type': 'text/plain'});
    var readableStream = fs.createReadStream("logo.gif");
    readableStream.pipe(response);
});
server.listen(9000);
